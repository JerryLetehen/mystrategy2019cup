using AiCup2019.Model;
using System;

namespace AiCup2019
{
    public class MyStrategy
    {
        int initHealth = 0;
        bool wounded = false;

        public UnitAction GetAction(Unit unit, Game game, Debug debug)
        {
            SetWounded(unit);
            AssignNearestEnemy(unit, game, out var nearestEnemy);
            AssignNearestWeapon(unit, game, out var nearestWeapon);
            AssignNearestHealthPack(unit, game, out var nearestHealth);
            var isBazuka = DoesNearesWeaponEqualBazuka(nearestWeapon);
            UnitAction action = new UnitAction();
            AssignTargetPos(new TargetPosData()
            {
                unit = unit,
                isBazuka = isBazuka,
                nearestEnemy = nearestEnemy,
                nearestWeapon = nearestWeapon,
                nearestHealth = nearestHealth
            }, out var targetPos, ref action);
            debug.Draw(new CustomData.Log($"Target pos: {targetPos.X}, {targetPos.Y}"));
            AssignAimPos(nearestEnemy, unit, out var aim);
            AssignJump(targetPos, unit, game, out var jump);
            bool isAimingWall = IsAimingWall(unit.Position, aim, game.Level, ref debug);

            
            action.Jump = true;
            action.JumpDown = !jump;
            action.Aim = aim;
            
            var a = nearestEnemy?.Position ?? new Vec2Double(0, 0);
            Shoot(ref action, unit, a);
            if (!action.Shoot)
            {
                targetPos = new Vec2Double(unit.Position.X + 5 * Sign(unit.Position, (nearestEnemy.HasValue ? nearestEnemy.Value.Position : new Vec2Double(0, 0))), unit.Position.Y);
            }
            
            if (nearestEnemy.HasValue)
            {
                action.Shoot = !isAimingWall;
            }
          
            action.Velocity = (targetPos.X - unit.Position.X) * 100;
            if (unit.Weapon.HasValue && action.Shoot)
            {
                action.Velocity = 0;
                action.Jump = true;
            }
            debug.Draw(new CustomData.Log($"Velocity: {action.Velocity}, Unit X: {unit.Position.X}"));
            action.Reload = false;
            
            action.PlantMine = false;
            return action;
        }

        private void Shoot(ref UnitAction action, Unit unit, Vec2Double targetPos)
        {
            action.Shoot = true;
            if (unit.Weapon.HasValue && unit.Weapon.Value.Typ == WeaponType.RocketLauncher)
            {
                if (DistanceSqr(targetPos, unit.Position) < 25)
                {
                    action.Shoot = false;
                }
            }
        }

        private bool IsAimingWall(Vec2Double current, Vec2Double aim, Level level, ref Debug debug)
        {
            var length = (int) GetLength(aim);
            debug.Draw(new CustomData.Log($"Length: {length}"));
            var dir = NormalizeVector(aim, length);
            current = new Vec2Double(current.X - Math.Sign(dir.X), current.Y);
            for (int i = 0; i < length; i++)
            {
                current = PlusVector(current, dir);
                if (DoesPointEqualWall(current, level))
                {
                    return true;
                }
            }
            return false;
        }

        static double DistanceSqr(Vec2Double a, Vec2Double b)
        {
            return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y);
        }

        static int Sign(Vec2Double a, Vec2Double b)
        {
            return a.X > b.X ? 1 : -1;
        }

        private void SetWounded(Unit unit)
        {
            if (unit.Health >= initHealth)
            {
                initHealth = unit.Health;
                wounded = false;
            }
            else
            {
                wounded = true;
            }
        }

        private void AssignNearestEnemy(Unit unit, Game game, out Unit? nearestEnemy)
        {
            nearestEnemy = null;
            foreach (var other in game.Units)
            {
                if (other.PlayerId != unit.PlayerId)
                {
                    if (!nearestEnemy.HasValue || DistanceSqr(unit.Position, other.Position) < DistanceSqr(unit.Position, nearestEnemy.Value.Position))
                    {
                        nearestEnemy = other;
                        return;
                    }
                }
            }
        }

        private void AssignNearestWeapon(Unit unit, Game game, out LootBox? nearestWeapon)
        {
            nearestWeapon = null;
            foreach (var lootBox in game.LootBoxes)
            {
                if (lootBox.Item is Item.Weapon)
                {
                    if (!nearestWeapon.HasValue || DistanceSqr(unit.Position, lootBox.Position) < DistanceSqr(unit.Position, nearestWeapon.Value.Position))
                    {
                        nearestWeapon = lootBox;
                    }
                }
            }
        }

        private void AssignNearestHealthPack(Unit unit, Game game, out LootBox? nearestHealth)
        {
            nearestHealth = null;
            foreach (var lootBox in game.LootBoxes)
            {
                if (lootBox.Item is Item.HealthPack)
                {
                    if (!nearestHealth.HasValue || DistanceSqr(unit.Position, lootBox.Position) < DistanceSqr(unit.Position, nearestHealth.Value.Position))
                    {
                        nearestHealth = lootBox;
                    }
                }
            }
        }

        private bool DoesNearesWeaponEqualBazuka(LootBox? nearestWeapon)
        {
            return nearestWeapon.HasValue && nearestWeapon.Value.Item is Item.Weapon && ((Item.Weapon) nearestWeapon.Value.Item).WeaponType == WeaponType.RocketLauncher;
        }

        private void AssignTargetPos(TargetPosData data, out Vec2Double targetPos, ref UnitAction action)
        {
            action.SwapWeapon = false;
            targetPos = new Vec2Double();
            if ((!data.unit.Weapon.HasValue && data.nearestWeapon.HasValue) || (data.unit.Weapon.HasValue && data.unit.Weapon.Value.Typ != WeaponType.RocketLauncher && data.isBazuka))
            {
                targetPos = data.nearestWeapon.Value.Position;
                action.SwapWeapon = true;
            }
            else if (wounded && data.nearestHealth.HasValue)
            {
                targetPos = data.nearestHealth.Value.Position;
            }
            else if (data.nearestEnemy.HasValue)
            {
                targetPos = data.nearestEnemy.Value.Position;
            }
        }

        private void AssignAimPos(Unit? nearestEnemy, Unit unit, out Vec2Double aim)
        {
            aim = new Vec2Double();
            if (nearestEnemy.HasValue)
            {
                aim = new Vec2Double(nearestEnemy.Value.Position.X - unit.Position.X, nearestEnemy.Value.Position.Y - unit.Position.Y);
            }
        }

        private void AssignJump(Vec2Double targetPos, Unit unit, Game game, out bool jump)
        {
            jump = false;
            bool isPlatformAbove = game.Level.Tiles[(int) unit.Position.X][(int) (unit.Position.Y + 1)] == Tile.Platform || game.Level.Tiles[(int) unit.Position.X][(int) (unit.Position.Y + 2)] == Tile.Platform;
            if (targetPos.X > unit.Position.X && game.Level.Tiles[(int) (unit.Position.X + 1)][(int) (unit.Position.Y)] == Tile.Wall)
            {
                jump = true;
            } 
            if (targetPos.X < unit.Position.X && game.Level.Tiles[(int) (unit.Position.X - 1)][(int) (unit.Position.Y)] == Tile.Wall)
            {
                jump = true;
            } 
            if (targetPos.Y > unit.Position.X && isPlatformAbove)
            {
                jump = true;
            }
        }

        private bool DoesPointEqualWall(Vec2Double point, Level level)
        {
            return level.Tiles[(int) point.X][(int) point.Y] == Tile.Wall;
        }

        private Vec2Double NormalizeVector(Vec2Double v, int length)
        {
            return DivideVector(v, length);
        }

        private double GetLength(Vec2Double a)
        {
            return Math.Abs(Math.Sqrt(DistanceSqr(a, new Vec2Double())));
        }

        private Vec2Double DivideVector(Vec2Double v, double a)
        {
            return new Vec2Double(v.X / a, v.Y / a);
        }

        private Vec2Double PlusVector(Vec2Double a, Vec2Double b)
        {
            return new Vec2Double(a.X + b.X, a.Y + b.Y);
        }
    }

    public struct TargetPosData
    {
        public Unit unit;
        public LootBox? nearestWeapon;
        public LootBox? nearestHealth;
        public Unit? nearestEnemy;
        public bool isBazuka;
    }
}